from django.db import models
import datetime

class Delivery(models.Model):
    JNEREG = 'JNE REG'
    JNEYES = 'JNE YES'
    SCSU = 'SCSU'
    SCSD = 'SCSD'
    COURIER_CHOICES = [
        (JNEREG, 'JNE REGULER'),
        (JNEYES, 'JNE YES'),
        (SCSU, 'SiCepat SiUntung'),
        (SCSD, 'SiCepat Same Day Delivery')
    ]
    STATUS_CHOICES = [
        ("ACTIVE", 'ACTIVE'),
        ("DONE", 'DONE')
    ]
    order_id = models.CharField(max_length=50, unique=True)
    username = models.CharField(max_length=50)
    courier = models.CharField(max_length=50, choices=COURIER_CHOICES, default=JNEREG)
    delivery_cost = models.IntegerField()
    delivery_start = models.DateTimeField(default=None, null=True, blank=True)
    delivery_finish = models.DateTimeField(default=None, null=True, blank=True)
    status = models.CharField(max_length=10, choices=STATUS_CHOICES, default="ACTIVE")
    destination_address = models.CharField(max_length=250)
    def __str__(self) -> str:
        return self.order_id

