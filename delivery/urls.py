from django.urls import path

from delivery.models import Delivery
from delivery.views import *

urlpatterns = [
    path('', DeliveryList.as_view()),
    path('<str:id>/', DeliveryID.as_view()),
    path('status/<str:del_status>/', DeliveryStatus.as_view()),
    path('orderid/<str:order_id>/', DeliveryOrderID.as_view())
]