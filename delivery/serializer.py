from rest_framework import serializers
from .models import Delivery
from django.db.models import fields

class DeliverySerializer(serializers.ModelSerializer):
    class Meta:
        model = Delivery
        fields = "__all__"

class CreateDeliverySerializer(serializers.ModelSerializer):
    class Meta:
        model = Delivery
        fields = ('order_id', 'username', 'courier', 'delivery_cost', 'destination_address')