from django.shortcuts import render
import datetime
import threading as th
import requests
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializer import DeliverySerializer, CreateDeliverySerializer
from .models import Delivery
from apscheduler.schedulers.background import BackgroundScheduler

def check_token(token) :
    url = 'http://tk.oauth.getoboru.xyz/token/resource'
    my_headers={'Authorization' : token}
    response = requests.get(url, headers = my_headers)
    if response.status_code == 200 :
        return True
    

class DeliveryList(APIView):
    def get(self, request):
        try:
            token_bearer = request.META['HTTP_AUTHORIZATION']
        except KeyError :
            return Response({
                "error" : "Not Authorized",
                "message" : "no token provided"
                }, status=status.HTTP_401_UNAUTHORIZED)
        if check_token(token_bearer):
            deliveries = Delivery.objects.all()
            serializer = DeliverySerializer(deliveries, many=True)
            if serializer:
                return Response(serializer.data)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({
                "error" : "Not Authorized",
                "message" : "wrong bearer token"
                }, status=status.HTTP_401_UNAUTHORIZED)
        
    def post(self, request):
        try:
            token_bearer = request.META['HTTP_AUTHORIZATION']
        except KeyError :
            return Response({
                "error" : "Not Authorized",
                "message" : "no token provided"
                }, status=status.HTTP_401_UNAUTHORIZED)
        if check_token(token_bearer):
            serializer = CreateDeliverySerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                delivery = Delivery.objects.get(order_id=serializer.data['order_id'])
                delivery.delivery_start = datetime.datetime.now() 
                delivery.save()
                object_id = getattr(delivery, 'id')
                update_delivery(request, object_id)
                return Response({
                "status": "succesful",
                "message": "Delivery with the id of " + str(object_id) + " has been created"
                }, status=status.HTTP_201_CREATED)
            else:
                return Response({
                "error" : "Cannot create object",
                "message" : "Invalid data"
                }, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({
                "error" : "Not Authorized",
                "message" : "wrong bearer token"
                }, status=status.HTTP_401_UNAUTHORIZED)

class DeliveryID(APIView):
    def get(self, request, id):
        try:
            token_bearer = request.META['HTTP_AUTHORIZATION']
        except KeyError :
            return Response({
                "error" : "Not Authorized",
                "message" : "no token provided"
                }, status=status.HTTP_401_UNAUTHORIZED)
        if check_token(token_bearer):
            try:
                deliveries = Delivery.objects.get(id=id)
            except Delivery.DoesNotExist:
                return Response({
                "message": "Delivery with the id " + id + " does not exist"
                }, status=status.HTTP_400_BAD_REQUEST)

            serializer = DeliverySerializer(deliveries, many=False)
            if serializer:
                return Response(serializer.data)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({
                "error" : "Not Authorized",
                "message" : "wrong bearer token"
                }, status=status.HTTP_401_UNAUTHORIZED)
    
    def put(self, request, id):
        try:
            deliveries = Delivery.objects.get(id=id)
        except Delivery.DoesNotExist:
            return Response({
            "message": "Delivery with the id " + id + " does not exist"
            }, status=status.HTTP_400_BAD_REQUEST)
        deliveries.delivery_finish = datetime.datetime.now()
        deliveries.status = "DONE"
        deliveries.save()
        return Response({
            "status": "succesful",
            "message": "Delivery with the id of " + str(id) + " has been updated"
            }, status=status.HTTP_200_OK)

    def delete(self, request, id):
        try:
            token_bearer = request.META['HTTP_AUTHORIZATION']
        except KeyError :
            return Response({
                "error" : "Not Authorized",
                "message" : "no token provided"
                }, status=status.HTTP_401_UNAUTHORIZED)
        if check_token(token_bearer):
            try:
                deliveries = Delivery.objects.get(id=id)
            except Delivery.DoesNotExist:
                return Response({
                "message": "Delivery with the id " + id + " does not exist"
                }, status=status.HTTP_400_BAD_REQUEST)
            if deliveries:
                deliveries.delete()
                return Response({
                "status": "succesful",
                "message": "Delivery with the id of " + id + " has been deleted"
                }, status=status.HTTP_200_OK)
        else:
            return Response({
                "error" : "Not Authorized",
                "message" : "wrong bearer token"
                }, status=status.HTTP_401_UNAUTHORIZED)

class DeliveryStatus(APIView):
    def get(self, request, del_status):
        try:
            token_bearer = request.META['HTTP_AUTHORIZATION']
        except KeyError :
            return Response({
                "error" : "Not Authorized",
                "message" : "no token provided"
                }, status=status.HTTP_401_UNAUTHORIZED)
        if check_token(token_bearer):
            active = Delivery.objects.filter(status=del_status)
            serializer = DeliverySerializer(active, many=True)
            if serializer:
                return Response(serializer.data)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({
                "error" : "Not Authorized",
                "message" : "wrong bearer token"
                }, status=status.HTTP_401_UNAUTHORIZED)

class DeliveryOrderID(APIView):
    def get(self, request, order_id):
        try:
            token_bearer = request.META['HTTP_AUTHORIZATION']
        except KeyError :
            return Response({
                "error" : "Not Authorized",
                "message" : "no token provided"
                }, status=status.HTTP_401_UNAUTHORIZED)
        if check_token(token_bearer):
            try:
                delivery = Delivery.objects.get(order_id=order_id)
            except Delivery.DoesNotExist:
                return Response({
                "message": "Delivery with the order id of " + order_id + " does not exist"
                }, status=status.HTTP_400_BAD_REQUEST)
            serializer = DeliverySerializer(delivery, many=False)
            if serializer:
                return Response(serializer.data)
            else:
                return Response(status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({
                "error" : "Not Authorized",
                "message" : "wrong bearer token"
                }, status=status.HTTP_401_UNAUTHORIZED)

def update_delivery(request, delivery_id):
    scheduler = BackgroundScheduler()
    delivery = DeliveryID()
    scheduler.add_job(delivery.put, "interval", minutes=5, args=[request, delivery_id], id='job_id_1')
    scheduler.start()
    T = th.Timer(310, scheduler.remove_job, ['job_id_1'])   
    T.start()   