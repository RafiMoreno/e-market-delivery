# Generated by Django 3.1.1 on 2022-05-24 17:30

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Delivery',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('order_id', models.CharField(max_length=50, unique=True)),
                ('username', models.CharField(max_length=50)),
                ('courier', models.CharField(choices=[('JNE REG', 'JNE REGULER'), ('JNE YES', 'JNE YES'), ('SiCepat SiUntung', 'SiCepat SiUntung'), ('SiCepat SameDay', 'SiCepat Same Day Delivery')], default='JNE REG', max_length=50)),
                ('delivery_cost', models.IntegerField()),
                ('delivery_start', models.DateTimeField(default=datetime.datetime(2022, 5, 25, 0, 30, 37, 815722))),
                ('status', models.CharField(choices=[('ACTIVE', 'ACTIVE'), ('DONE', 'DONE')], default='ACTIVE', max_length=10)),
                ('destination_address', models.CharField(max_length=250)),
            ],
        ),
    ]
