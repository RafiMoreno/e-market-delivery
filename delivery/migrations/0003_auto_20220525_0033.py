# Generated by Django 3.1.1 on 2022-05-24 17:33

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('delivery', '0002_auto_20220525_0032'),
    ]

    operations = [
        migrations.AlterField(
            model_name='delivery',
            name='delivery_start',
            field=models.DateTimeField(default=datetime.datetime(2022, 5, 25, 0, 33, 58, 83898)),
        ),
    ]
