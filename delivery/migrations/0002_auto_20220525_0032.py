# Generated by Django 3.1.1 on 2022-05-24 17:32

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('delivery', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='delivery',
            name='delivery_start',
            field=models.DateTimeField(default=datetime.datetime(2022, 5, 25, 0, 32, 30, 369585)),
        ),
    ]
